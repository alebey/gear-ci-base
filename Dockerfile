FROM python:3.7

RUN apt-get update && \
    apt-get install -y gcc libev-dev musl-dev python3-all-dev && \
    pip3 install --upgrade pip setuptools && \
    git clone https://github.com/pyenv/pyenv.git ~/.pyenv && \
    export PYENV_ROOT="$HOME/.pyenv" && \
    export PATH="$PYENV_ROOT/bin:$PATH" && \
    pyenv install 3.6.4 && \
    ln -s ~/.pyenv/versions/3.6.4/bin/python3.6 /usr/bin/python3.6

# Note: tox relies on py.path.local.sysfind to find python3.5, python3.6, and python3.7.
#       This base image has python3.5 located in /usr/bin, and python3.7 located in /usr/local/bin
#       We use pyenv to install python3.6 but py.path.local.sysfind fails to find it
#       adding a symlink in /usr/bin to the pyenv installation of python3.6 fixes the problem
